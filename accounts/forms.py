from django import forms
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
    )
from django.forms import Textarea,TextInput

User = get_user_model()
class UserLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Логин','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным'}, label="")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Пароль','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным'}, label="")

    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]
        # labels = {
        #     "username": "",
        #     "password": "",
        # }

        # widgets = {
        #     'username': TextInput(attrs={'placeholder': 'Логин'}),
        #     'password': TextInput(attrs={'placeholder': 'Пароль'}),
        # }

    def clean(self,*args,**kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
         
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("Неправильный логин или пароль")
            if not user.check_password(password):
                raise forms.ValidationError("Неправильный пароль")
            if not user.is_active:
                raise forms.ValidationError("Пользователь с таким именем улетел в бан")
        return super(UserLoginForm,self).clean(*args,**kwargs)

class UserRegisterForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Логин','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным',"username_exists": "Username already present"}, label="")
    email= forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным','invalid': 'Некорректный email'},label="")
    # email2 = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Повторите email'}),error_messages={'required': 'Это поле является обязательным','invalid': 'Некорректный email'},label="")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Пароль','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным'},label="")
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Повторите пароль','class' : 'formclasslogin'}),error_messages={'required': 'Это поле является обязательным'},label="")
    # password = forms.CharField(widget=forms.PasswordInput,label="", widget=forms.TextInput(attrs={'placeholder': 'Пароль'}))

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            # 'email2',
            'password',
            'password2',
        ]
        # labels = {
        #     "username": "",
        #     "password": "",
        #     "email": "",
        #     "email2": "",
        # }

        # widgets = {
        #     # 'username': TextInput(attrs={'placeholder': 'Логин'}),
        #     # 'password': TextInput(attrs={'placeholder': 'Пароль'}),
        #     # 'email': TextInput(attrs={'placeholder': 'Email'}),
        #     # 'email2': TextInput(attrs={'placeholder': 'Повторите email'}),
        # }


    # def clean_email(self):
    #     email =  self.cleaned_data.get('email')
    #     email2 =  self.cleaned_data.get('email')
    #     if email != email:
    #         raise forms.ValidationError("Email не совпалают")
    #     email_qs = User.objects.filter(email=email)# все 
    #     if email_qs.exists():
    #         raise forms.ValidationError("Этот email уже существует")
    #     return super(UserRegisterForm,self).clean(*args, **kwargs)
    # def clean_username(self):
    #     username = self.cleaned_data.get("username")

    #     try:
    #         User.objects.filter(username=username)
    #         raise forms.ValidationError(
    #             self.error_messages['username_exists'],#my error message
    #             code='username_exists',#set the error message key
    #             )
    #     except User.DoesNotExist:
    #         return username # if user does not exist so we can continue the registration proces

    # def clean_email2(self):
    #     email =  self.cleaned_data.get('email')
    #     email2 =  self.cleaned_data.get('email2')
    #     if email != email2:
    #         raise forms.ValidationError("Email не совпадают")
    #     email_qs = User.objects.filter(email=email)# все 
    #     if email_qs.exists():
    #         raise forms.ValidationError("Этот email уже существует")
    #     return email

    def clean_email(self):
        email =  self.cleaned_data.get('email')
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError("Этот email уже существует")
        return email

    def clean_password2(self):
        password =  self.cleaned_data.get('password')
        password2 =  self.cleaned_data.get('password2')
        if password != password2:
            raise forms.ValidationError("Пароли не совпадают")
        return password