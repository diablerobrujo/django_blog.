from django import forms
from django.forms import Textarea,TextInput

class CommentForm(forms.Form):
	content_type = forms.CharField(widget=forms.HiddenInput)
	object_id = forms.IntegerField(widget=forms.HiddenInput)
	content = forms.CharField(widget=forms.Textarea(),error_messages={'required': 'Это поле является обязательным'} )
