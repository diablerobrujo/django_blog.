from django import forms 
 
from django.forms import Textarea,TextInput
from pagedown.widgets import PagedownWidget

from .models import Post



class PostForm(forms.ModelForm):
    content  = forms.CharField(widget=PagedownWidget(), label="",error_messages={'required': 'Это поле является обязательным'})
    # publish  = forms.DateField(widget=forms.SelectDateWidget(), label="Дата публикации")
    title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Заголовок','cols': 2, 'rows': 2}),label="",error_messages={'required': 'Это поле является обязательным'})
    class Meta:
        model = Post
        fields = [
            "title",
            "content",
            "draft",
            # "publish"
        ]
        labels = {
        "title": "",
        "content": "",
        "draft": "Черновик",
        "publish": "Дата публикации",
        "image": "Картинка",
        }
 
        widgets = {
            'title': Textarea(attrs={'placeholder': 'Заголовок','cols': 2, 'rows': 2}),
            'content': Textarea(attrs={'placeholder': 'Контент'}),
        }
        error_messages= {
        'required': 'Это поле является обязательным'
        }

    def clean(self,*args,**kwargs):
        title = self.cleaned_data.get("title")