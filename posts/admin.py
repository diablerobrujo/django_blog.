from django.contrib import admin

from .models import Post, Category

class PostModelAdmin(admin.ModelAdmin):
    list_display = ["__unicode__","timestamp"]
    class Meta:
        model = Post

class CategoryModelAdmin(admin.ModelAdmin):
    # list_display = ["__unicode__","timestamp"]
    class Meta:
        model = Category
admin.site.register(Category,CategoryModelAdmin)
admin.site.register(Post, PostModelAdmin)