from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from comments.models import Comment, CommentManager
from urllib.parse import (
    ParseResult, SplitResult, _coerce_args, _splitnetloc, _splitparams, quote,
    quote_plus, scheme_chars, unquote, unquote_plus,
    urlencode as original_urlencode, uses_params,
)
from comments.form import CommentForm
from .forms import PostForm
from .models import Post, Category

def category_list(request,slug):
    category = get_object_or_404(Category, slug=slug) 
    context = {
            "category": category,
            "posts": Post.objects.filter(category=category)
        }
    return render(request,"base.html", context )


def post_list(request):
    today = timezone.now().date()
    queryset_list = Post.objects.active()
    if request.user.is_staff or request.user.is_superuser:
        queryset_list = Post.objects.all()
    
    query = request.GET.get("q")
    if query:
        queryset_list = queryset_list.filter(
            Q(title__icontains=query) | 
            Q(content__icontains=query)
            ).distinct()
    paginator = Paginator(queryset_list, 9) # Show 9 contacts per page
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
            "object_list" : queryset, 
            "title" : "List",
            "today" : today,
        }
    return render(request,"post_list.html", context )


def post_create(request):
    if request.user.is_staff or request.user.is_superuser:
        form = PostForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            messages.success(request,"Пост успешно сохранён")
            return HttpResponseRedirect(instance.get_absolute_url())
        # else:
        #     messages.error(request,"Не получилось сохранить пост")
        context = {
            "form": form,
            "head_title": "Создать пост"
            }
        return render(request,"post_form.html", context)
    else:
        raise Http404
        
def post_detail(request, slug=None):
    instance = get_object_or_404(Post, slug=slug)
    if instance.publish > timezone.now().date() or instance.draft:
        if not request.user.is_staff or  not request.user.is_superuser:
            raise Http404
    share_string = quote_plus(instance.content)
    initial_data = {
        "content_type": instance.get_content_type,
        "object_id": instance.id
    }

    form = CommentForm(request.POST or None, initial=initial_data)
    if form.is_valid():
        c_type = form.cleaned_data.get("content_type")
        content_type = ContentType.objects.get(model=c_type)
        obj_id = form.cleaned_data.get("object_id")
        content_data = form.cleaned_data.get("content")
        parent_obj = None
        try:
            parent_id = request.POST.get("parent_id")
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count()==1:
                parent_obj = parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
            user  = request.user,
            content_type  = content_type,
            object_id = obj_id,
            content = content_data,
            parent = parent_obj,
            )
        return HttpResponseRedirect(new_comment.content_object.get_absolute_url())

    comments = Comment.objects.filter_by_instance(instance)
    context = {
        "title" : instance.title,
        "instance" : instance,
        "share_string": share_string,
        "comments": comments,
        "comment_form": form,
    }
    return render(request,"post_detail.html", context )

def post_update(request, slug=None):
    if request.user.is_staff or request.user.is_superuser:
        instance = get_object_or_404(Post, slug=slug)
        form = PostForm(request.POST or None, request.FILES or None, instance=instance)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            messages.success(request,"Пост успешно сохранён")
            return HttpResponseRedirect(instance.get_absolute_url())
        context = {
            "title": instance.title,
            "instance": instance,
            "form": form,
            "head_title": "Редактировать пост"
        }
        return render(request, "post_form.html", context)
    else:
        raise Http404

def post_delete(request, slug=None):
    # if not request.user.is_staff or not request.user.is_superuser:
    #     raise Http404

    # instance = get_object_or_404(Post, slug=slug)
    # instance.delete()
    # messages.success(request, "Пост успешно удалён")
    # return redirect("/")
    if request.user.is_staff or request.user.is_superuser:
        instance = get_object_or_404(Post, slug=slug)
        instance.delete()
        # messages.success(request, "Пост успешно удалён")
        return redirect("/")
    else:
        raise Http404

